# Reservoir Docs : Un talk sur Docusaurus

La présentation est déployée automatiquement en ligne, et est disponible ici : https://gasouch.gitlab.io/talk-docusaurus/

## Démarrer la présentation en local

Pour démarrer la présentation en local : 

```bash
git clone git@gitlab.com:gasouch/talk-docusaurus.git

cd talk-docusaurus && hugo server
```

## Liens utiles

* Ticket CFP : https://gitlab.maxds.fr/apps/tooling/maxcamp/-/issues/144
* Site officiel Docusaurus : https://docusaurus.io/fr/
* Thème présentation : https://github.com/dzello/reveal-hugo



Idées de points à aborder : 

* sections (blog (post mortem, actus), ADR)
