+++
title = "Reservoir Docs"
outputs = ["Reveal"]
[logo]
src = "Logo-MAX-couleur.png"
alt = "Max logo"
margin = 0.2
+++

{{<slide background-image="reservoir-docs.png" background-size="contain" background-position="0% 0%">}}

<div style="margin-left: 60%; font-size: large">
    <img style="border-radius:50%; width:10em;" src="mat_souch.jpg">
    <div style="display:flex; justify-content:center; align-items:center;">
        <img src="twitter-icon.png" style="border:none; background:transparent; margin-right:15px;" />➡&nbsp&nbsp&nbsp<a href="https://twitter.com/Gasouch">@Gasouch</a>
    </div>
    <div style="display:flex; justify-content:center; align-items:center;">
        <img src="max-blog.png" style="width: 100px; border:none; background:transparent; margin-right:8px;" />➡&nbsp&nbsp&nbsp<a href="https://blog.maxds.fr/authors/mathieu-souchet/">Quelques articles de blogs</a>
    </div>
    <div style="display:flex; justify-content:center; align-items:center;">
        <img src="gitlab-logo.png" style="width: 50px; border:none; background:transparent; margin-right:8px;" />➡&nbsp&nbsp&nbsp<a href="https://gitlab.com/users/gasouch/projects">Mon Gitlab public</a>
    </div>
</div>

{{% note %}}
* Moi : Développeur Back, un peu Devops

* En mission chez HelloWork ➡ Découverte de Docusaurus

* Objectifs : Prez / RETEX / Bootstrap
{{% /note %}}

---

{{% section %}}

{{<slide background-image="elle-est-ou-la-doc.jpg" background-opacity="0.8" background-position="0% 0%">}}

## Elle est où la doc ??

<br/>

{{% fragment %}}C'est compliqué 😣{{% /fragment %}}

{{% note %}}
* Est-ce qu'il y a quelqu'un ici, qui ne s'est jamais posé cette question en arrivant sur un projet ?

* Réponse classique : Confluence, Wiki, Readme, Spec Word dans une GED/Drive, CR de réunion, Swagger-UI pour les clients de l'API ... Ou tout simplement : Y'en a pas 

* Documentation produit / Documentation équipe ... 

C'EST PAS SIMPLE
{{% /note %}}

---

{{<slide background-image="suspicious.jpg" background-opacity="0.8" background-position="30%">}}

## C'est quoi ton excuse ??

<br/>

{{% fragment %}}Pas le temps{{% /fragment %}}
{{% fragment %}}Ca n'intéressera personne{{% /fragment %}}
{{% fragment %}}C'est ennuyeux à faire{{% /fragment %}}
{{% fragment %}}Je ferai ça plus tard{{% /fragment %}}
{{% fragment %}}...{{% /fragment %}}

---

Mais il est chiant avec sa doc,

j'croyais qu'il était codeur moi !

<img alt="doc-as-code" src="talk-doc-as-code.png" style="width: 65%"/>

{{% note %}}
* Je vous rassure, je ne suis pas PO, je suis bien codeur

* Alors oui, j'en parle souvent, car la doc, c'est important ! (on doit transmettre nos connaissances, on est presta, bus factor toussa)

* Lobbyiste de solutions "as-code" pour la doc / slides (cette prez c'est du code 😁)
{{% /note %}}

{{% /section %}}

---

{{% section %}}

{{<slide background-image="docusaurus-background.png" background-opacity="0.5" background-position="0% 20%">}}

## Docusaurus

[🔗 Docusaurus.io](https://docusaurus.io/fr/)

<img alt="doc-as-code" src="docusaurus-home-page.png" style="width: 65%"/>

{{% note %}}
* Créé par Facebook en 2018

* Outil open source qui permet la création d'une documentation à partir de fichiers Markdown

Démo : (Docusaurus.io)

* Home page / Menu / Footer customizable
* Sections = répertoires
* Pages = fichiers markdown
* Thème clair / foncé
* Support des langues (internationalisation)
* Possibilité d'avoir un espace de blog (pour publier des changelog, postmortem)

{{% /note %}}

---

{{<slide background-image="under-the-hood.png" background-opacity="0.4">}}

## Sous le capot

<br/>

<p style="display: flex; align-items: center; justify-content: center">
    <img src="react.svg" style="width: 200px; margin-right: 20px;"/>
    <span>Construit avec React</span>
</p>
<p style="display: flex; align-items: center; justify-content: center">
    <span>Support de <code>.md</code> & <code>.mdx</code></span>
    <img src="mdx.svg" style="width: 200px; margin-left: 20px;"/>
</p>

{{% note %}}
* Single page application

* React embedded in Markdown
{{% /note %}}

---

## Features 

<p>
    <span class="fragment" style="display: flex; align-items: center; justify-content: center">
        <img src="react.svg" style="height: 70px;"/>
        <span>Possibilité de créer ses propres composants React</span>
    </span>
</p>
<p>
    <span class="fragment" style="display: flex; align-items: center; justify-content: center">
        <span>Entièrement modulable via des plugins</span>
        <img src="plugin.png" style="height: 60px; margin-left: 10px;"/>
    </span>
</p>
<p>
    <span class="fragment" style="display: flex; align-items: center; justify-content: center">
        <img src="around-the-world.svg" style="height: 70px; margin-right: 10px;"/>
        <span>Support des langues (Internationalization i18n)</span>
    </span>
</p>
<p>
    <span class="fragment" style="display: flex; align-items: center; justify-content: center">
        <span>Moteur de recherche Algolia</span>
        <img src="algolia.svg" style="height: 70px; margin-left: 10px;"/>
    </span>
</p>
{{% fragment %}}💾&nbsp;&nbsp;&nbsp;Versioning de la documentation{{% /fragment %}}

{{% note %}}
* Tout est plugin dans Docusaurus, la partie doc est un plugin, la partie blog est un plugin ... On peut virer ce qui ne nous sert pas. Y'a 2 catalogues dispo (core team / communauté)

* Le versioning pratique pour garder la doc sur les versions correspondantes du produit
{{% /note %}}

---

{{<slide background-image="docusaurus-add-plugin.png" background-opacity="0.3">}}

## Facile à installer 

<br/>

1️⃣ Installer `Node.js`

2️⃣ Créer un nouveau site Docusaurus :

```shell
npx create-docusaurus@latest my-website classic
```

3️⃣ Démarrer le site :

```shell
cd my-website
npx docusaurus start
```

---

{{<slide background-image="docusaurus-add-plugin.png" background-opacity="0.3">}}

## Installer un plugin

<br/>

1️⃣ Installer le plugin via `npm` :

```shell
npm install --save docusaurus-plugin-name
```

<br/>

2️⃣ L'ajouter dans la config Docusaurus `docusaurus.config.js` :

```js
module.exports = {
    plugins: [
        // ...
        ['@docusaurus/plugin-xxx', { /* options */ }]
    ]
};
```

---

{{<slide background-image="demo-time.gif" background-opacity="0.5">}}

## Demo time !

{{% note %}}
```shell
npx docusaurus start
```
* Ajouter une page ➡ Constater la prise à chaud
```shell
npm install --save @easyops-cn/docusaurus-search-local
```
* Ajouter ça dans la conf : 
```js
  plugins: [
    [
      require.resolve("@easyops-cn/docusaurus-search-local"),
      /** @type {import("@easyops-cn/docusaurus-search-local").PluginOptions} */
      ({
        hashed: true
      })
    ]
  ],
```
* Exécuter :
```shell
npm run build
npm run serve
```
{{% /note %}}

---

{{<slide background-image="technical-team.png" background-opacity="0.3">}}

## Pour qui ? Pour quoi ?

<br/>

{{% fragment %}}➡ Solution Doc-as-code (Markdown & Git){{% /fragment %}}
{{% fragment %}}➡ Vos données restent dans votre instance Git (⧣ Confluence SaaS){{% /fragment %}}
{{% fragment %}}➡ Solution technique oui, mais pour tout type de documentation{{% /fragment %}}

{{% note %}}
* Par forcément réservé aux documentations techniques, peut servir aux documentations d'équipe, documentations fonctionnelles

* Docusaurus excelle là où Confluence/Word sont limités (saisie hors-ligne, versioning, extraits de code, schemas-as-code)
{{% /note %}}

--- 

{{<slide background-image="promesses.jpg" background-opacity="0.5" background-position="60%">}}

## Quelques promesses

<br/>

{{% fragment %}}➡ Rédiger la doc dans votre IDE favori{{% /fragment %}}
{{% fragment %}}➡ Au plus près de votre code applicatif{{% /fragment %}}
{{% fragment %}}➡ Déployée rapidement (content focused){{% /fragment %}}
{{% fragment %}}➡ Rendu web{{% /fragment %}}
{{% fragment %}}➡ Plein de plugins additionnels dispos{{% /fragment %}}

{{% note %}}
* conseil : La doc produit, dans le repo du produit

* conseil : la doc de l'équipe, dans un repo spécifique du groupe Gitlab de l'équipe

* Installation rapide, pas de configuration

* Utilise le moteur React pour générer un site statique mono page (comme hugo par ex.)
{{% /note %}}

{{% /section %}}

---

{{<slide background-image="5150.gif" background-opacity="0.5">}}

## À vous de jouer

{{% note %}}
* Des questions ?
{{% /note %}}

